#Report

|  #    | Stage                  | Start date |  End date  | Comment
||----------------------------------------------------------------------------------------------------------------
|| 1    | Task Clarification     | 26.05.2023 | 27.05.2023 | 
|| 2    | Analyze                | 26.05.2023 | 28.05.2023 | Analyzed the file
|| 3    | Initial Commit         | 28.05.2023 | 28.05.2023 | Created the blank project
|| 4    | Software development   | 29.05.2023 | 08.06.2023 |
|| 5    | Main layer             | 30.05.2023 | 08.06.2023 | Added info of author and linked to controller
|| 6    | Dto layer              | 31.05.2023 | 08.06.2023 | Written code for reading csv file
|| 7    | ControllerDao layer    | 02.06.2023 | 08.06.2023 | Written switch cases for using the code
|| 8    | Services layer         | 04.06.2023 | 08.06.2023 | Written methods for every variables
|| 9    | Test                   | 06.06.2023 | 08.06.2023 | Written test for find out if everything works correctly
|| 10   | Presentation           | 08.06.2023 | 09.06.2023 | 
