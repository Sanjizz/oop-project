package org.example;

import org.example.DAO.DaoControllerImpl;
import org.example.DAO.DaoControllerInterface;
import org.example.DTO.DtoData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class DaoControllerImplTest {
    private DaoControllerInterface daoController;

    @BeforeEach
    public void setUp() {
        daoController = new DaoControllerImpl();
    }

    @Test
    public void testReturnAllProductsFromCsv() {
        String path = "src/test/resources/Clothes.csv";
        List<String[]> csvData = new ArrayList<>();
        csvData.add(new String[]{"1", "Product 1", "Category 1", "S", "10", "Male", "10.00"});
        csvData.add(new String[]{"2", "Product 2", "Category 2", "M", "20", "Female", "20.00"});

        List<DtoData> expected = new ArrayList<>();
        expected.add(new DtoData("1", "Product 1", "Category 1", "S", "10", "Male", "10.00"));
        expected.add(new DtoData("2", "Product 2", "Category 2", "M", "20", "Female", "20.00"));

        List<DtoData> result = daoController.returnAllProductsFromCsv(path);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testReturnFilteredProductsFromCsv() {
        String path = "src/test/resources/Footwear.csv";
        List<String[]> csvData = new ArrayList<>();
        csvData.add(new String[]{"1", "Product 1", "Category 1", "S", "10", "Male", "10.00"});
        csvData.add(new String[]{"2", "Product 2", "Category 2", "M", "20", "Female", "20.00"});

        List<DtoData> expected = new ArrayList<>();
        expected.add(new DtoData("1", "Product 1", "Category 1", "S", "10", "Male", "10.00"));

        List<DtoData> result = daoController.returnFilteredProductsFromCsv(path);

        Assertions.assertEquals(expected, result);
    }
}
