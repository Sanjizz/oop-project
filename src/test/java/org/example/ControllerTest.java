package org.example;

import org.example.Service.ServiceInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ControllerTest {
    private ServiceInterface service;

    @BeforeEach
    public void setUp() {
        service = new ServiceStub();
    }

    @Test
    public void testMainShowAllProducts() {
        main.path = "path/to/csv";
        assertTrue(service.showAllProducts(main.path));
    }

    @Test
    public void testMainSearchByParameter() {
        main.path = "path/to/csv";
        assertTrue(service.searchByParameter(main.path));
    }

    // Stub implementation of ServiceInterface for testing
    private static class ServiceStub implements ServiceInterface {
        @Override
        public boolean showAllProducts(String path) {
            // Simulate behavior for ShowAllProducts method
            // Return true to indicate success
            return true;
        }

        @Override
        public boolean searchByParameter(String path) {
            // Simulate behavior for SearchByParameter method
            // Return true to indicate success
            return true;
        }
    }
}
