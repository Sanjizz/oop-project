package org.example.DAO;

import org.example.DTO.DtoData;
import java.util.List;

public interface DaoControllerInterface {
    List <DtoData> returnAllProductsFromCsv(String path);
    List <DtoData> returnFilteredProductsFromCsv(String path);
}
