package org.example.DAO;



import org.example.DTO.DtoData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DaoControllerImpl implements DaoControllerInterface{


    @Override
    public List <DtoData> returnAllProductsFromCsv(String path) {
        List <String[]> resultString = csvFileToStringArray(path);
        removeHeader(resultString);
        List <DtoData> resultDto = stringArrayToDtoArray(resultString);
        return resultDto;
    }

    @Override
    public List <DtoData> returnFilteredProductsFromCsv(String path){
        List <String[]> resultString = csvFileToStringArray(path);
        removeHeader(resultString);
        List <DtoData> resultDtoArray = filterbyParameter(resultString);
        return resultDtoArray;
    }

    private void removeHeader(List<String[]> stringList) {
        for(int i = 0; i < stringList.size () - 1; i ++){
            stringList.set (i, stringList.get (i + 1));
        }
        stringList.remove (stringList.size () - 1);
    }

    private List <String[]> csvFileToStringArray(String path) {
        String line = "";       // для чтения каждой строки csv файла
        String splitBy = ",";   //знак для разделения строки на параметры
        List <String[]> products;
        BufferedReader csvProducts;
        try {
            csvProducts = new BufferedReader (new FileReader (path));
            products = new ArrayList <> ();

            while ((line = csvProducts.readLine ()) != null) {
                String[] temporaryProduct = line.split (splitBy);

                products.add (temporaryProduct);
            }
        } catch (Exception e) {
            System.out.println("File was not found");
            throw new RuntimeException (e);
        }
        return products;
    }


    private List <DtoData> stringArrayToDtoArray(List <String[]> stringList) {
        List <DtoData> result = new ArrayList <> ();

        for (int i = 0; i < stringList.size (); i++) {
            String[] lines = stringList.get (i);
            DtoData response = new DtoData ();

            response.setId (lines[0]);
            response.setName (lines[1]);
            response.setCategory (lines[2]);
            response.setSize (lines[3]);
            response.setQuantity (lines[4]);
            response.setGender (lines[5]);
            response.setPrice (lines[6]);

            result.add (response);
        }

        return result;
    }

    private List <DtoData> filterbyParameter(List<String[]> stringList){
        Scanner scan = new Scanner (System.in);
        List <DtoData> result = new ArrayList <> ();
        int intOfParameter;
        String searchParameterValue;
        outLoop:
        while(true) {
            intOfParameter = -1;
            System.out.println ("By which parameter do you want to search ?\n" +
                    "1 - id\n" +
                    "2 - name\n" +
                    "3 - category\n" +
                    "4 - size\n" +
                    "5 - quantity\n" +
                    "6 - gender\n" +
                    "7 - price\n");
            String input = scan.nextLine ();
            switch (input) {
                case "1": {
                    intOfParameter = 0;
                    break outLoop;
                }
                case "2": {
                    intOfParameter = 1;
                    break outLoop;
                }
                case "3": {
                    intOfParameter = 2;
                    break outLoop;
                }
                case "4": {
                    intOfParameter = 3;
                    break outLoop;
                }
                case "5": {
                    intOfParameter = 4;
                    break outLoop;
                }
                case "6": {
                    intOfParameter = 5;
                    break outLoop;
                }
                case "7": {
                    intOfParameter = 6;
                    break outLoop;
                }
                default: {
                    System.out.println ("Enter correct value!");
                    break;
                }
            }

        }

        System.out.println ("Enter the value: ");
        searchParameterValue = scan.nextLine ();
        //cheking conditions
        for (int i = 0; i < stringList.size (); i++) {
            String[] lines = stringList.get (i);
            if(lines[intOfParameter].equalsIgnoreCase (searchParameterValue)) {
                DtoData response = new DtoData ();

                response.setId (lines[0]);
                response.setName (lines[1]);
                response.setCategory (lines[2]);
                response.setSize (lines[3]);
                response.setQuantity (lines[4]);
                response.setGender (lines[5]);
                response.setPrice (lines[6]);

                result.add (response);
            }
        }

        return result;
    }
    public void addProductToCsv(String path, DtoData newProduct) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, true))) {
            // Appending the new product to the end of the CSV file
            writer.write(newProduct.getId()  + "," +
                    newProduct.getName() + "," +
                    newProduct.getCategory() + "," +
                    newProduct.getSize() + "," +
                    newProduct.getQuantity() + "," +
                    newProduct.getGender() + "," +
                    newProduct.getPrice() + "\n");

            System.out.println("Product added to CSV successfully.");
        } catch (IOException e) {
            System.out.println("Error adding product to CSV: " + e.getMessage());
        }
    }

    public void deleteProductFromCsv(String path, List<DtoData> updatedProducts) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            // Writing the updated product list to the CSV file
            for (DtoData product : updatedProducts) {
                writer.write(product.getId() + "," +
                        product.getName() + "," +
                        product.getCategory() + "," +
                        product.getSize() + "," +
                        product.getQuantity() + "," +
                        product.getGender() + "," +
                        product.getPrice() + "\n");
            }

            System.out.println("Product deleted from CSV successfully.");
        } catch (IOException e) {
            System.out.println("Error deleting product from CSV: " + e.getMessage());
        }
    }

    public void addCategoryToCsv(String path, String newCategory) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, true))) {
            // Appending the new category to the end of the CSV file
            writer.write(",,," + newCategory + ",,,\n");

            System.out.println("Category added to CSV successfully.");
        } catch (IOException e) {
            System.out.println("Error adding category to CSV: " + e.getMessage());
        }
    }


}


