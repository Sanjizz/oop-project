package org.example.DTO;

public class  DtoData {
    private String id;
    private String name;
    private String category;
    private String size;
    private String quantity;
    private String gender;
    private String price;

    public DtoData (String id, String name, String category, String size, String quantity, String gender, String price){
        this.id = id;
        this.name = name;
        this.category = category;
        this.size = size;
        this.quantity = quantity;
        this.gender = gender;
        this.price = price;
    }
    public DtoData(){

    }


    public void Print() {
        System.out.println ("product: id: " + id
                + " name: " + name
                + " category: " + category
                + " size: " + size
                + " quantity: " + quantity
                + " gender: " + gender
                + " price: " + price);
    }

    public void setId(String id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setCategory(String category){
        this.category = category;
    }
    public void setSize(String size){
        this.size = size;
    }
    public void setQuantity(String quantity){
        this.quantity = quantity;
    }
    public void setGender(String gender){
        this.gender = gender;
    }
    public void setPrice(String price){
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getSize() {
        return size;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getGender() {
        return gender;
    }

    public String getPrice() {
        return price;
    }
}