package org.example.Admin;

import org.example.DAO.DaoControllerImpl;
import org.example.DTO.DtoData;

import java.util.List;
import java.util.Scanner;

public class AdminPanel {
    private final DaoControllerImpl daoController = new DaoControllerImpl();

    public void startAdminPanel(String path) {
        org.example.Admin.Admin admin = new org.example.Admin.Admin();

        if (admin.authenticate()) {
            System.out.println("Authentication successful. Welcome to Admin Panel!");

            Scanner scanner = new Scanner(System.in);

            while (true) {
                System.out.println("\nAdmin Panel Menu:");
                System.out.println("1 - Add product to CSV");
                System.out.println("2 - Delete product from CSV");
                System.out.println("3 - Add category to CSV");
                System.out.println("4 - Exit Admin Panel");

                String input = scanner.nextLine();

                switch (input) {
                    case "1":
                        addProductToCSV(path);
                        break;
                    case "2":
                        deleteProductFromCSV(path);
                        break;
                    case "3":
                        addCategoryToCSV(path);
                        break;
                    case "4":
                        System.out.println("Exiting Admin Panel.");
                        return;
                    default:
                        System.out.println("Invalid option. Please choose a valid option.");
                }
            }
        } else {
            System.out.println("Authentication failed. Exiting Admin Panel.");
        }
    }

    private void addProductToCSV(String path) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter product details:");

        System.out.print("ID: ");
        String id = scanner.nextLine();

        System.out.print("Name: ");
        String name = scanner.nextLine();

        System.out.print("Category: ");
        String category = scanner.nextLine();

        System.out.print("Size: ");
        String size = scanner.nextLine();

        System.out.print("Quantity: ");
        String quantity = scanner.nextLine();

        System.out.print("Gender: ");
        String gender = scanner.nextLine();

        System.out.print("Price: ");
        String price = scanner.nextLine();

        DtoData newProduct = new DtoData(id, name, category, size, quantity, gender, price);

        // Now you can add the newProduct to the CSV file using daoController
        daoController.addProductToCsv(path, newProduct);
    }

    private void deleteProductFromCSV(String path) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter product details to delete:");

        System.out.print("Enter product ID: ");
        String productIdToDelete = scanner.nextLine();

        // Get all products from the CSV file
        List<DtoData> products = daoController.returnAllProductsFromCsv(path);

        // Check if the product with the specified ID exists
        boolean productFound = false;
        for (DtoData product : products) {
            if (product.getId().equalsIgnoreCase(productIdToDelete)) {
                productFound = true;
                // Display details of the product to be deleted
                System.out.println("Product to delete:");
                product.Print();

                // Confirm deletion
                System.out.print("Do you want to delete this product? (yes/no): ");
                String confirmation = scanner.nextLine().toLowerCase();

                if (confirmation.equals("yes")) {
                    // Remove the product from the list
                    products.remove(product);

                    // Update the CSV file with the modified list
                    daoController.deleteProductFromCsv(path, products);

                    System.out.println("Product deleted successfully.");
                } else {
                    System.out.println("Deletion canceled.");
                }
                break;  // No need to continue searching
            }
        }

        if (!productFound) {
            System.out.println("Product not found with the specified ID.");
        }
    }


    private void addCategoryToCSV(String path) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter category details:");

        System.out.print("Category Name: ");
        String categoryName = scanner.nextLine();

        // Now you can add the new category to the CSV file using daoController
        daoController.addCategoryToCsv(path, categoryName);

        System.out.println("Category added successfully.");
    }

}
