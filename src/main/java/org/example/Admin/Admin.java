package org.example.Admin;

import java.util.Scanner;

public class Admin {
    private final String adminUsername = "admin";
    private final String adminPassword = "admin123";

    public boolean authenticate() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Admin Username:");
        String usernameInput = scanner.nextLine();

        System.out.println("Enter Admin Password:");
        String passwordInput = scanner.nextLine();

        return adminUsername.equals(usernameInput) && adminPassword.equals(passwordInput);
    }
}
