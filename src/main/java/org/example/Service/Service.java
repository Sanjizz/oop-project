package org.example.Service;

import org.example.DAO.DaoControllerImpl;
import org.example.DAO.DaoControllerInterface;
import org.example.DTO.DtoData;

import java.util.List;

public class Service implements ServiceInterface{


    DaoControllerInterface daoControllerInterface = new DaoControllerImpl ();
    @Override
    public boolean showAllProducts(String path){
        List <DtoData> dtoList = daoControllerInterface.returnAllProductsFromCsv (path);
        printByDtoArray(dtoList);
        return false;
    }

    @Override
    public boolean searchByParameter(String path){
        List <DtoData> dtoList = daoControllerInterface.returnFilteredProductsFromCsv (path);
        if (dtoList.size() != 0) printByDtoArray(dtoList);
        else System.out.println("Nothing was found for your query");
        return false;
    }


    public void printByDtoArray(List<DtoData> dtoList){
        for(DtoData dtoObject : dtoList){
            dtoObject.Print ();
        }
    }
}
