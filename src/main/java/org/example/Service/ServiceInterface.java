package org.example.Service;

public interface ServiceInterface {
    public boolean showAllProducts(String path);
    public boolean searchByParameter(String path);
}
