package org.example.controller;


import org.example.Service.Service;
import org.example.Service.ServiceInterface;
import org.example.Admin.AdminPanel;

import java.util.Scanner;

public class ControllerImpl {
    public static String pathInventory = "src/main/resources/Clothes.csv";
    public static String pathFootwear = "src/main/resources/Footwear.csv";
    public static String path = "src/main/resources/Product.csv";
    static Scanner scan = new Scanner(System.in);

    static ServiceInterface service = new Service();

    public void choose_operation() {
        System.out.println("""
                |Project name : Clothe and Footwear                             |
                |Creation date : 26.05.2023                                     |
                |Version of project : v1.5                                      |
                |Developer info : Sanjar Pulatov, Sanjar_Pulatov@student.itpu.uz|
                """);

        while (true) {
            outerLoop:
            while (true) {
                System.out.println("\nChoose category:\n1 - Clothes\n2 - Footwear\n3 - Exit app\n4 - AdminPanel");

                String input = scan.nextLine();

                switch (input) {
                    case "1": {
                        path = pathInventory;
                        break outerLoop;
                    }
                    case "2": {
                        path = pathFootwear;
                        break outerLoop;
                    }
                    case "3": {
                        return;
                    }
                    case "4": {
                        AdminPanel adminPanel = new AdminPanel();
                        adminPanel.startAdminPanel(path);
                        break;
                    }
                    default: {
                        System.out.println("Type correct operation");
                        break;
                    }
                }
            }


            System.out.println("\nChoose operation:\n1 - show all products" +
                    "\n2 - search by parameter\n3 - exit app");
            String input = scan.nextLine();
            switch (input) {
                case "1" -> service.showAllProducts(path);
                case "2" -> service.searchByParameter(path);
                case "3" -> {
                    return;
                }
                default -> System.out.println("Type correct operation");
            }
        }
    }

}
